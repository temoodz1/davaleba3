package com.person.kalkulatiori

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        button0.setOnClickListener {
            calc(button0)
        }
        button1.setOnClickListener {
            calc(button1)
        }
        button2.setOnClickListener {
            calc(button2)
        }
        button3.setOnClickListener {
            calc(button3)
        }
        button4.setOnClickListener {
            calc(button4)
        }
        button5.setOnClickListener {
            calc(button5)
        }
        button6.setOnClickListener {
            calc(button6)
        }
        button7.setOnClickListener {
            calc(button7)
        }
        button8.setOnClickListener {
            calc(button8)
        }
        button9.setOnClickListener {
            calc(button9)
        }
        buttonPlus.setOnClickListener {
            calc(buttonPlus)

        }
        buttonMinus.setOnClickListener {
            calc(buttonMinus)
        }
        buttonGay.setOnClickListener {
            calc(buttonGay)
        }
        buttonGamr.setOnClickListener {
            calc(buttonGamr)
        }
        buttonEqual.setOnClickListener {
            calc(buttonEqual)
        }
    }

    private fun calc(button: Button) {
        buttondisplay.text = buttondisplay.text.toString() + button.text

    }

}